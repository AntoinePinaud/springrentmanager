package com.ensta.rentmanager.service;

import com.ensta.rentmanager.dao.ClientDao;
import com.ensta.rentmanager.dao.ReservationDao;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Reservation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ReservationServiceTest {

    @InjectMocks
    ReservationService reservationService;

    @Mock
    ReservationDao reservationDao;

    Reservation reservationTest;
    List<Reservation> reservationList;

    @Before
    public void init() {
        this.reservationTest = new Reservation(1, 2, LocalDate.of(2000, 10, 10),  LocalDate.of(2000, 10, 10));
        this.reservationList = new ArrayList<>();
        this.reservationList.add(new Reservation(1, 2, LocalDate.of(2000, 10, 10),  LocalDate.of(2000, 10, 10)));
        this.reservationList.add(new Reservation(2, 2, LocalDate.of(2000, 10, 10), LocalDate.of(2000, 10, 10)));
    }
    @Test
    public void create_with_valid_reservation_should_return_valid_id() throws DaoException, ServiceException {
        when(reservationDao.create(this.reservationTest)).thenReturn(1L);
        assertEquals(1L, reservationService.create(this.reservationTest));
    }
    @Test(expected = ServiceException.class)
    public void create_with_invalid_reservation_should_return_exception() throws DaoException, ServiceException {
        when(reservationDao.create(this.reservationTest)).thenThrow(DaoException.class);
        reservationService.create(this.reservationTest);
    }

    @Test
    public void find_all_should_return_list_of_reservations() throws DaoException, ServiceException {
        when(reservationDao.findAll()).thenReturn(this.reservationList);
        assertNotNull(reservationService.findAll());
    }
    @Test(expected = ServiceException.class)
    public void find_all_should_return_exception() throws DaoException, ServiceException {
        when(reservationDao.findAll()).thenThrow(DaoException.class);
        List<Reservation> list = reservationService.findAll(); }
}
