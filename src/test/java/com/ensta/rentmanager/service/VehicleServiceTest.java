package com.ensta.rentmanager.service;

import com.ensta.rentmanager.dao.VehicleDao;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Vehicule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VehicleServiceTest {

    @InjectMocks
    VehicleService vehicleService;

    @Mock
    VehicleDao vehicleDao;

    Vehicule vehicleTest;
    List<Vehicule> vehicleList;

    @Before
    public void init() {
        this.vehicleTest = new Vehicule(1, "Test", "Test", 3);
        this.vehicleList = new ArrayList<>();
        this.vehicleList.add(new Vehicule(1, "Test1", "Test1", 3));
        this.vehicleList.add(new Vehicule(2, "Test2", "Test2",  3));
    }

    @Test
    public void create_with_valid_vehicle_should_return_valid_id() throws DaoException, ServiceException {
        when(vehicleDao.create(this.vehicleTest)).thenReturn(1L);
        assertEquals(1L, vehicleService.create(this.vehicleTest));
    }

    @Test(expected = ServiceException.class)
    public void create_with_invalid_vehicle_should_return_exception() throws DaoException, ServiceException {
        when(vehicleDao.create(this.vehicleTest)).thenThrow(DaoException.class);
        vehicleService.create(this.vehicleTest);
    }

    @Test
    public void find_by_id_should_return_existing_vehicle() throws DaoException, ServiceException {
        when(vehicleDao.findById(this.vehicleTest.getId())).thenReturn(Optional.of(this.vehicleTest));
        assertEquals(this.vehicleTest, vehicleService.findById(this.vehicleTest.getId()));
    }
    @Test(expected = ServiceException.class)
    public void find_by_id_should_return_exception() throws DaoException, ServiceException {
        when(vehicleDao.findById(this.vehicleTest.getId())).thenThrow(DaoException.class);
        vehicleService.findById(this.vehicleTest.getId()); }
    @Test
    public void find_all_should_return_list_of_vehicless() throws DaoException, ServiceException {
        when(vehicleDao.findAll()).thenReturn(this.vehicleList);
        assertNotNull(vehicleService.findAll());
    }
    @Test(expected = ServiceException.class)
    public void find_all_should_return_exception() throws DaoException, ServiceException {
        when(vehicleDao.findAll()).thenThrow(DaoException.class);
        List<Vehicule> list = vehicleService.findAll(); }
}

