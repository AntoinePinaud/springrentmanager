package com.ensta.rentmanager.service;

import com.ensta.rentmanager.dao.ClientDao;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceTest {

    @InjectMocks
    ClientService clientService;

    @Mock
    ClientDao clientDao;

    Client clientTest;
    List<Client> clientList;

    @Before
    public void init() {
        this.clientTest = new Client(1, "Test", "Test", "Test", LocalDate.of(2000, 10, 10));
        this.clientList = new ArrayList<>();
        this.clientList.add(new Client(1, "Test1", "Test1", "Test1", LocalDate.of(2000, 10, 10)));
        this.clientList.add(new Client(2, "Test2", "Test2", "Test2", LocalDate.of(2000, 10, 10)));
    }
    @Test
    public void create_with_valid_client_should_return_valid_id() throws DaoException, ServiceException {
        when(clientDao.create(this.clientTest)).thenReturn(1L);
        assertEquals(1L, clientService.create(this.clientTest));
    }
    @Test(expected = ServiceException.class)
    public void create_with_invalid_client_should_return_exception() throws DaoException, ServiceException {
        when(clientDao.create(this.clientTest)).thenThrow(DaoException.class);
        clientService.create(this.clientTest);
    }

    @Test
    public void find_by_id_should_return_existing_client() throws DaoException, ServiceException {
        when(clientDao.findById(this.clientTest.getId())).thenReturn(Optional.of(this.clientTest));
        assertEquals(this.clientTest, clientService.findById(this.clientTest.getId()));
    }
    @Test(expected = ServiceException.class)
    public void find_by_id_should_return_exception() throws DaoException, ServiceException {
        when(clientDao.findById(this.clientTest.getId())).thenThrow(DaoException.class);
        clientService.findById(this.clientTest.getId()); }
    @Test
    public void find_all_should_return_list_of_clients() throws DaoException, ServiceException {
        when(clientDao.findAll()).thenReturn(this.clientList);
        assertNotNull(clientService.findAll());
    }
    @Test(expected = ServiceException.class)
    public void find_all_should_return_exception() throws DaoException, ServiceException {
        when(clientDao.findAll()).thenThrow(DaoException.class);
        List<Client> list = clientService.findAll(); }
}
