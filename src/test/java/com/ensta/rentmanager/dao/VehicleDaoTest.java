/*package com.ensta.rentmanager.dao;

import com.ensta.database_utils.persistence.ConnectionManager;;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.model.Vehicule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VehicleDaoTest {

    @InjectMocks
    VehicleDao vehicleDao;

    @Mock
    ConnectionManager connectionManager;

    Vehicule vehicleTest;
    List<Vehicule> vehicleList;

    @Before
    public void init() {
        this.vehicleTest = new Vehicule(1, "Test", "Test", 3);
        this.vehicleList = new ArrayList<>();
        this.vehicleList.add(new Vehicule(1, "Test1", "Test1", 3));
        this.vehicleList.add(new Vehicule(2, "Test2", "Test2", 3));
    }

    @Test(expected = DaoException.class)
    public void create_with_invalid_connection_should_return_exception() throws SQLException, DaoException {
        when(connectionManager.getConnection()).thenThrow(SQLException.class);
        vehicleDao.create(this.vehicleTest);
    }
}*/