package com.ensta.rentmanager.mapper;

import com.ensta.rentmanager.dto.ReservationDatabaseDto;
import com.ensta.rentmanager.model.Reservation;
import org.springframework.stereotype.Component;

@Component
public class ReservationDatabaseMapper {

    public Reservation mapToEntity(ReservationDatabaseDto reservationDatabaseDto) {
        Reservation reservation = new Reservation();
        reservation.setId(reservationDatabaseDto.getId());
        reservation.setClient_id(reservationDatabaseDto.getClient_id());
        reservation.setVehicle_id(reservationDatabaseDto.getVehicle_id());
        reservation.setDebut(reservationDatabaseDto.getDebut());
        reservation.setFin(reservationDatabaseDto.getFin());
        return reservation;
    }
    public ReservationDatabaseDto mapToDto(Reservation reservation) {
        ReservationDatabaseDto reservationDatabaseDto = new ReservationDatabaseDto();
        reservationDatabaseDto.setId(reservation.getId());
        reservationDatabaseDto.setClient_id(reservation.getClient_id());
        reservationDatabaseDto.setVehicle_id(reservation.getVehicle_id());
        reservationDatabaseDto.setDebut(reservation.getDebut());
        reservationDatabaseDto.setFin(reservation.getFin());
        return reservationDatabaseDto;
    }
}
