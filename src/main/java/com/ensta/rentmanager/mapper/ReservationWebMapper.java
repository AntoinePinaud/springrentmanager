package com.ensta.rentmanager.mapper;

import com.ensta.rentmanager.dto.ReservationWebDto;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Reservation;
import com.ensta.rentmanager.model.Vehicule;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class ReservationWebMapper {

    private ClientService clientService;
    private VehicleService vehicleService;

    @Autowired
    private ReservationWebMapper(ClientService clientService, VehicleService vehicleService) {

        this.clientService = clientService;
        this.vehicleService = vehicleService;

    }

    public ReservationWebMapper() {
    }

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public Reservation mapToEntity(ReservationWebDto reservationWebDto) {
        Reservation reservation = new Reservation();
        reservation.setClient_id(reservationWebDto.getClient_id());
        reservation.setVehicle_id(reservationWebDto.getVehicle_id());
        reservation.setDebut(LocalDate.parse(reservationWebDto.getDebut(), formatter));
        reservation.setFin(LocalDate.parse(reservationWebDto.getFin(), formatter));
        return reservation;
    }

    public ReservationWebDto mapToDto(Reservation reservation) throws ServiceException {
        ReservationWebDto reservationWebDto = new ReservationWebDto();
        reservationWebDto.setId(reservation.getId());
        reservationWebDto.setDebut(reservation.getDebut().format(formatter));
        reservationWebDto.setFin(reservation.getFin().format(formatter));
        Client client = this.clientService.findById(reservation.getClient_id());
        Vehicule vehicle = this.vehicleService.findById((reservation.getVehicle_id()));
        reservationWebDto.setConstructeur(vehicle.getConstructeur());
        reservationWebDto.setModel(vehicle.getModel());
        reservationWebDto.setPrenom(client.getPrenom());
        reservationWebDto.setNom(client.getNom());
        reservationWebDto.setClient(client);
        reservationWebDto.setVehicle(vehicle);

        return reservationWebDto;
    }
}
