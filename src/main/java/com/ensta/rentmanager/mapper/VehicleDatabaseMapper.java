package com.ensta.rentmanager.mapper;

import com.ensta.rentmanager.dto.VehicleDatabaseDto;
import com.ensta.rentmanager.dto.VehicleWebDto;
import com.ensta.rentmanager.model.Vehicule;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Component
public class VehicleDatabaseMapper {

    public Vehicule mapToEntity(VehicleDatabaseDto vehicleDatabaseDto) {
        Vehicule vehicle = new Vehicule();
        vehicle.setId(vehicleDatabaseDto.getId());
        vehicle.setModel(vehicleDatabaseDto.getModel());
        vehicle.setConstructeur(vehicleDatabaseDto.getConstructeur());
        vehicle.setNb_places(vehicleDatabaseDto.getNb_places());
        return vehicle;
    }
    public VehicleDatabaseDto mapToDto(Vehicule vehicle) {
        VehicleDatabaseDto vehicleDatabaseDto = new VehicleDatabaseDto();
        vehicleDatabaseDto.setId(vehicle.getId());
        vehicleDatabaseDto.setModel(vehicle.getModel());
        vehicleDatabaseDto.setConstructeur(vehicle.getConstructeur());
        vehicleDatabaseDto.setNb_places(vehicle.getNb_places());
        return vehicleDatabaseDto;
    }
}
