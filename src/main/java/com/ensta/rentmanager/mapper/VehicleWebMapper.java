package com.ensta.rentmanager.mapper;


import com.ensta.rentmanager.dto.VehicleWebDto;
import com.ensta.rentmanager.model.Vehicule;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Component
public class VehicleWebMapper {

    public VehicleWebMapper() {
    }

    public Vehicule mapToEntity(VehicleWebDto vehicleWebDto) {
        Vehicule vehicle = new Vehicule();
        vehicle.setModel(vehicleWebDto.getModel());
        vehicle.setConstructeur(vehicleWebDto.getConstructeur());
        vehicle.setNb_places(vehicleWebDto.getNb_places());
        return vehicle;
    }
    public VehicleWebDto mapToDto(Vehicule vehicle) {
        VehicleWebDto vehicleWebDto = new VehicleWebDto();
        vehicleWebDto.setId(vehicle.getId());
        vehicleWebDto.setModel(vehicle.getModel());
        vehicleWebDto.setConstructeur(vehicle.getConstructeur());
        vehicleWebDto.setNb_places(vehicle.getNb_places());
        return vehicleWebDto;
    }
}
