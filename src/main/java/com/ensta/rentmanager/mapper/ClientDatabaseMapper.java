package com.ensta.rentmanager.mapper;

import com.ensta.rentmanager.dto.ClientDatabaseDto;
import com.ensta.rentmanager.dto.VehicleDatabaseDto;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Vehicule;
import org.springframework.stereotype.Component;

@Component
public class ClientDatabaseMapper {

    public Client mapToEntity(ClientDatabaseDto clientDatabaseDto) {
        Client client = new Client();
        client.setId(clientDatabaseDto.getId());
        client.setNom(clientDatabaseDto.getNom());
        client.setPrenom(clientDatabaseDto.getPrenom());
        client.setEmail(clientDatabaseDto.getEmail());
        client.setNaissance(clientDatabaseDto.getNaissance());
        return client;
    }
    public ClientDatabaseDto mapToDto(Client client) {
        ClientDatabaseDto clientDatabaseDto = new ClientDatabaseDto();
        clientDatabaseDto.setId(client.getId());
        clientDatabaseDto.setNom(client.getNom());
        clientDatabaseDto.setPrenom(client.getPrenom());
        clientDatabaseDto.setEmail(client.getEmail());
        clientDatabaseDto.setNaissance(client.getNaissance());
        return clientDatabaseDto;
    }
}
