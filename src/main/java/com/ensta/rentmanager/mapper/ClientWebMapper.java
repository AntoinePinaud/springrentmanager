package com.ensta.rentmanager.mapper;

import com.ensta.rentmanager.dto.ClientWebDto;
import com.ensta.rentmanager.dto.VehicleWebDto;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Vehicule;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class ClientWebMapper {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public Client mapToEntity(ClientWebDto clientWebDto) {
        Client client = new Client();
        client.setNom(clientWebDto.getNom());
        client.setPrenom(clientWebDto.getPrenom());
        client.setEmail(clientWebDto.getEmail());
        client.setNaissance(LocalDate.parse(clientWebDto.getNaissance(), formatter));
        return client;
    }
    public ClientWebDto mapToDto(Client client) {
        ClientWebDto clientWebDto = new ClientWebDto();
        clientWebDto.setId(client.getId());
        clientWebDto.setNom(client.getNom());
        clientWebDto.setPrenom(client.getPrenom());
        clientWebDto.setEmail(client.getEmail());
        clientWebDto.setNaissance(client.getNaissance().format(formatter));
        return clientWebDto;
    }
}
