package com.ensta.rentmanager.ui.servlet;

import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
/*@WebServlet("/home")*/
public class HomeServlet extends HttpServlet {

	@Autowired
	private ClientService clientService;
	@Autowired
	private VehicleService vehicleService;
	@Autowired
	private ReservationService reservationService;

	@Override
	public void init() throws ServletException {
		super.init();
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int clientCount = 0;
		int vehicleCount = 0;
		int reservationCount = 0;
		try {
			clientCount = this.clientService.count();
			vehicleCount = this.vehicleService.count();
			reservationCount = this.reservationService.count();
		} catch (DaoException e) {
		}
		request.setAttribute("clientCount", clientCount);
		request.setAttribute("reservationCount", reservationCount);
		request.setAttribute("vehicleCount", vehicleCount);
		RequestDispatcher rd=request.getRequestDispatcher("/WEB-INF/views/home.jsp");
		rd.forward(request, response);
	}
}

