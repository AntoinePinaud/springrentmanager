package com.ensta.rentmanager.ui.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Vehicule;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/*@WebServlet("/cars/create")*/
public class VehicleCreateServlet extends HttpServlet {

	@Autowired
	private VehicleService service;

	@Override
	public void init() throws ServletException {
		super.init();
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd=request.getRequestDispatcher("/WEB-INF/views/vehicles/create.jsp"); 
		rd.forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String marque = request.getParameter("brand");
		String model = request.getParameter("model");
		int nb_places = Integer.valueOf(request.getParameter("seats"));
		Vehicule vehicule = new Vehicule(marque, model, nb_places);
		try {
			this.service.create(vehicule);
		} catch (ServiceException e) {}
		RequestDispatcher rd=request.getRequestDispatcher("/WEB-INF/views/home.jsp"); 
		rd.forward(request, response);
		
		
		
	}
}