package com.ensta.rentmanager.ui.servlet;

import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

/*WebServlet("rents/create")*/
public class ReservationCreateServlet extends HttpServlet {

    @Autowired
    private ReservationService service;

    @Override
    public void init() throws ServletException {
        super.init();
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

}
