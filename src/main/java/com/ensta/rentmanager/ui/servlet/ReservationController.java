package com.ensta.rentmanager.ui.servlet;

import com.ensta.rentmanager.dto.ReservationWebDto;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.mapper.ClientWebMapper;
import com.ensta.rentmanager.mapper.ReservationWebMapper;
import com.ensta.rentmanager.mapper.VehicleWebMapper;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Reservation;
import com.ensta.rentmanager.model.Vehicule;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/rents")
public class ReservationController {

    private ReservationService reservationService;
    private ReservationWebMapper reservationWebMapper;
    private ClientService clientService;
    private ClientWebMapper clientWebMapper;
    private VehicleService vehicleService;
    private VehicleWebMapper vehicleWebMapper;

    public ReservationController(ReservationService reservationService, ReservationWebMapper reservationWebMapper,
                                 ClientService clientService, ClientWebMapper clientWebMapper,
                                 VehicleService vehicleService, VehicleWebMapper vehicleWebMapper) {
        this.reservationWebMapper = reservationWebMapper;
        this.reservationService = reservationService;
        this.clientService = clientService;
        this.clientWebMapper = clientWebMapper;
        this.vehicleService = vehicleService;
        this.vehicleWebMapper = vehicleWebMapper;
    }

    @GetMapping /** GET /rents **/
    public String getall(Model model) {
        try {
            List<Reservation> list = this.reservationService.findAll();
            List<ReservationWebDto> newList = new ArrayList<ReservationWebDto>(list.size());
            for (Reservation reservation : list) {
                newList.add(this.reservationWebMapper.mapToDto(reservation));
            }
            model.addAttribute("rents", newList);
        } catch (ServiceException e) { ;
            System.out.println(e);
        }

        return "/rents/list";
    }

    @GetMapping("/error") /** GET /rents **/
    public String getallError(Model model) {
        try {
            List<Reservation> list = this.reservationService.findAll();
            List<ReservationWebDto> newList = new ArrayList<ReservationWebDto>(list.size());
            for (Reservation reservation : list) {
                newList.add(this.reservationWebMapper.mapToDto(reservation));
            }
            model.addAttribute("rents", newList);
            model.addAttribute("msg", "Erreur ! La réservation n'a pas pu être supprimé !");
        } catch (ServiceException e) { ;
            System.out.println(e);
        }

        return "/rents/list";
    }

    @GetMapping("/create")  /** GET /rents/create **/
    public String create(Model model) throws ServiceException {
        List<Client> listClient = this.clientService.findAll();
        model.addAttribute("clients", listClient);
        List<Vehicule> listVehicle = this.vehicleService.findAll();
        model.addAttribute("cars", listVehicle);
        return "/rents/create";
    }

    @GetMapping("/create/error")  /** GET /rents/create **/
    public String createError(Model model) throws ServiceException {
        List<Client> listClient = this.clientService.findAll();
        model.addAttribute("clients", listClient);
        List<Vehicule> listVehicle = this.vehicleService.findAll();
        model.addAttribute("cars", listVehicle);
        model.addAttribute("msg", "Erreur ! La réservation n'a pas pu être créée !");
        return "/rents/create";
    }

    @GetMapping("/delete/{reservation_id}")
    public RedirectView delete(Model model, @PathVariable(name = "reservation_id") long reservation_id) throws ServiceException {
        try {
            Reservation reservation = this.reservationService.findById(reservation_id);
            this.reservationService.delete(reservation);
        } catch (ServiceException e) {
            System.out.println(e);
            return new RedirectView("/RentManager/rents/error");
        }
        return new RedirectView("/RentManager/rents");
    }

    @PostMapping("/create")  /** Post /rents/create **/
    public RedirectView create(Model model, @ModelAttribute() final ReservationWebDto reservation) {
        try {
            this.reservationService.create(this.reservationWebMapper.mapToEntity(reservation));
        } catch (ServiceException e) {
            return new RedirectView("/RentManager/rents/create/error");
        }

        return new RedirectView("/RentManager/rents");
    }
}

