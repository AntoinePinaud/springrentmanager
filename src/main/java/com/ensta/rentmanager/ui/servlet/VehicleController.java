package com.ensta.rentmanager.ui.servlet;

import com.ensta.rentmanager.dto.VehicleWebDto;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.mapper.VehicleWebMapper;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Vehicule;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
@RequestMapping("/cars")
public class VehicleController {

    private VehicleService vehicleService;

    private VehicleWebMapper vehicleWebMapper;

    @Autowired
    public VehicleController(VehicleService vehicleService, VehicleWebMapper vehicleWebMapper) {

        this.vehicleService = vehicleService;
        this.vehicleWebMapper = vehicleWebMapper;
    }

    @GetMapping /** GET /cars **/
    public String getall(Model model) {
        try {
            List<Vehicule> list = this.vehicleService.findAll();
            model.addAttribute("vehicles", list);
        } catch (ServiceException e) {
            System.out.println(e);
        }

        return "/vehicles/list";
    }

    @GetMapping("/error") /** GET /cars **/
    public String getallError(Model model) {
        try {
            List<Vehicule> list = this.vehicleService.findAll();
            model.addAttribute("vehicles", list);
            model.addAttribute("msg", "Erreur ! Le véhicule n'a pas pu être supprimé !");
        } catch (ServiceException e) {
            System.out.println(e);
        }

        return "/vehicles/list";
    }

    @GetMapping("/create")  /** GET /cars/create **/
    public String create(Model model) {

        return "/vehicles/create";
    }

    @GetMapping("/create/error")  /** GET /cars/create/error **/
    public String createError(Model model) {
        model.addAttribute("msg", "Erreur ! Le véhicule n'a pas pu être créé !");
        return "/vehicles/create";
    }

    @GetMapping("/delete/{vehicule_id}")  /** GET /cars/deletion/ **/
    public RedirectView delete(Model model, @PathVariable(name = "vehicule_id") long vehicule_id) throws ServiceException {
        try {
            Vehicule vehicule = this.vehicleService.findById((vehicule_id));
            this.vehicleService.delete(vehicule);
        } catch (ServiceException e) {
            System.out.println(e);
            return new RedirectView("/RentManager/cars/error");
        }
        return new RedirectView("/RentManager/cars");
    }

    @PostMapping("/create")  /** Post /cars/create **/
    public RedirectView create(Model model, @ModelAttribute() final VehicleWebDto vehicle) {
        try {
            this.vehicleService.create(this.vehicleWebMapper.mapToEntity(vehicle));
        } catch (ServiceException e) {
            return new RedirectView("/RentManager/cars/create/error");
        }

        return new RedirectView("/RentManager/cars");
    }

}
