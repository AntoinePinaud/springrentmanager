package com.ensta.rentmanager.ui.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Controller
/*@WebServlet("/users")*/
public class ClientListServlet extends HttpServlet {

	@Autowired
	private ClientService service;

	@Override
	public void init() throws ServletException {
		super.init();
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			List<Client> list = this.service.findAll();
			request.setAttribute("users", list);
			} catch (ServiceException e) { 
				response.getWriter().write("<div>error message</div>");
			}
		RequestDispatcher rd=request.getRequestDispatcher("/WEB-INF/views/users/list.jsp"); 
		rd.forward(request, response);
	}
}
