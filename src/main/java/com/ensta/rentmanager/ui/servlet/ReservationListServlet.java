package com.ensta.rentmanager.ui.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Reservation;
import com.ensta.rentmanager.model.Vehicule;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Controller
/*@WebServlet("/rents")*/
public class ReservationListServlet extends HttpServlet {

	@Autowired
	private ReservationService service;

	@Override
	public void init() throws ServletException {
		super.init();
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			List<Reservation> list = this.service.findAll();
			request.setAttribute("reservations", list);
			} catch (ServiceException e) { 
				response.getWriter().write("<div>error message</div>");
			}
		RequestDispatcher rd=request.getRequestDispatcher("/WEB-INF/views/rents/list.jsp"); 
		rd.forward(request, response);
	}
}
