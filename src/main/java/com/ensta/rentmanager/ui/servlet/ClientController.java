package com.ensta.rentmanager.ui.servlet;

import com.ensta.rentmanager.dto.ClientWebDto;
import com.ensta.rentmanager.dto.ReservationDatabaseDto;
import com.ensta.rentmanager.dto.ReservationWebDto;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.mapper.ClientWebMapper;
import com.ensta.rentmanager.mapper.ReservationWebMapper;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Reservation;
import com.ensta.rentmanager.service.ClientService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/users")
public class ClientController {

    private ClientService clientService;
    private ClientWebMapper clientWebMapper;
    private ReservationWebMapper reservationWebMapper;

    public ClientController(ClientService clientService, ClientWebMapper clientWebMapper, ReservationWebMapper reservationWebMapper) {
        this.clientWebMapper = clientWebMapper;
        this.clientService = clientService;
        this.reservationWebMapper = reservationWebMapper;
    }

    @GetMapping /** GET /users **/
    public String getall(Model model) {
        try {
            List<Client> list = this.clientService.findAll();
            model.addAttribute("users", list);
        } catch (ServiceException e) { ;
            System.out.println(e);
        }

        return "/users/list";
    }

    @GetMapping("/error") /** GET /users **/
    public String getallError(Model model) {
        try {
            List<Client> list = this.clientService.findAll();
            model.addAttribute("users", list);
            model.addAttribute("msg", "Erreur ! L'utilisateur n'a pas pu être supprimé !");
        } catch (ServiceException e) { ;
            System.out.println(e);
        }

        return "/users/list";
    }

    @GetMapping("/create")  /** GET /users/create **/
    public String create(Model model) {

        return "/users/create";
    }

    @GetMapping("/create/error")  /** GET /users/create/error **/
    public String createError(Model model) {
        model.addAttribute("msg", "Erreur ! L'utilisateur n'a pas pu être créé !");
        return "/users/create";
    }

    @GetMapping("/delete/{client_id}")
    public RedirectView delete(Model model, @PathVariable(name = "client_id") long client_id) throws ServiceException {
        try {
            Client client = this.clientService.findById((client_id));
            this.clientService.delete(client);
        } catch (ServiceException e) {
            System.out.println(e);
            return new RedirectView("/RentManager/users/error");

        }
        return new RedirectView("/RentManager/users");
    }

    @GetMapping("/profile/{client_id}")
    public String profile(Model model, @PathVariable(name = "client_id") long client_id) throws ServiceException {
        try {
            Client client = this.clientService.findById((client_id));
            List<Reservation> rents = this.clientService.findReservations(client_id);
            List<ReservationWebDto> webRents = new ArrayList<>();
            for (Reservation reservation : rents) {
                webRents.add(this.reservationWebMapper.mapToDto(reservation));
            }
            long count = rents.size();
            model.addAttribute("client", client);
            model.addAttribute("count", count);
            model.addAttribute("rents", webRents);

        } catch (ServiceException | DaoException e) {
            System.out.println(e);
        }
        return "/users/details";
    }


    @PostMapping("/create")  /** Post /users/create **/
    public RedirectView create(Model model, @ModelAttribute() final ClientWebDto client) {
        try {
            this.clientService.create(this.clientWebMapper.mapToEntity(client));
        } catch (ServiceException e) {
            return new RedirectView("/RentManager/users/create/error");
        }

        return new RedirectView("/RentManager/users");
    }
}
