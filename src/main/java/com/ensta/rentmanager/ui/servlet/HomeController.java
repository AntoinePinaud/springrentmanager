package com.ensta.rentmanager.ui.servlet;

import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/home")
public class HomeController {

    private final VehicleService vehicleService;
    private final ClientService clientService;
    private final ReservationService reservationService;

    public HomeController(VehicleService vehicleService, ClientService clientService, ReservationService reservationService) {
        this.vehicleService = vehicleService;
        this.clientService = clientService;
        this.reservationService = reservationService;
    }

    @GetMapping /** GET /home **/
    public String get(Model model) {
        int clientCount = 0;
        int vehicleCount = 0;
        int reservationCount = 0;
        try {
            clientCount = this.clientService.count();
            vehicleCount = this.vehicleService.count();
            reservationCount = this.reservationService.count();
        } catch (DaoException e) {
        }

        model.addAttribute("clientCount", clientCount);
        model.addAttribute("reservationCount", reservationCount);
        model.addAttribute("vehicleCount", vehicleCount);

        return "/home";
    }
}
