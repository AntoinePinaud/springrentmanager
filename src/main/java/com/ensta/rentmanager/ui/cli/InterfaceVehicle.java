package com.ensta.rentmanager.ui.cli;

import java.util.List;

import com.ensta.rentmanager.configuration.AppConfiguration;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Vehicule;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.VehicleService;
import com.ensta.rentmanager.utils.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class InterfaceVehicle {

    @Autowired
    private VehicleService service;

	
	public static void create() {
        System.out.println("Creation d'un nouveau véhicule");
        // String input 
        System.out.println("Constructeur:");
        String constructeur = IOUtils.readString();
        System.out.println("Modele:");
        String modele = IOUtils.readString();
        int nb_places = IOUtils.readInt("Nombre de places : ");
        Vehicule vehicule = new Vehicule(constructeur, modele, nb_places);
        try {
            VehicleService service = VehicleService.getInstance();
			service.create(vehicule);
		} catch (ServiceException e) {
		}	
        }
	
	public static void list() {
        try {
            VehicleService service = VehicleService.getInstance();
			List <Vehicule> vehicles = service.findAll();
	       for(Vehicule vehicle : vehicles) {
	            System.out.println(vehicle);
	        }
		} catch (ServiceException e) {
		}
	}
	
	public static void exec()
    { 
        // Declare the object and initialize with 
        // predefined standard input object 
        System.out.println("Bienvenu dans l'interface véhicule !");
        System.out.println("Souhaitez-vous créer un véhicule ou lister tous les véhicules? (create/list)");
        // String input 
        boolean bool = false;
        while (!bool) {
        String function = IOUtils.readString(); 
    	if (function.equalsIgnoreCase("create")) {
        	InterfaceVehicle.create();
        	bool = true;
        }
        else if (function.equalsIgnoreCase("list")) {
        	InterfaceVehicle.list();
        	bool = true;
        }
        else {
        	System.out.println("Entrée incorrecte");
            System.out.println("Souhaitez-vous créer un véhicule ou lister tous les véhicules? (create/list)");
        }

        }
        System.out.println("Fin de l'accès aux données");
   }
}
