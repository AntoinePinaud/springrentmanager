package com.ensta.rentmanager.ui.cli;


import com.ensta.rentmanager.utils.IOUtils;

public class CommandLineInterface {
	
	public static void main(String[] args) 
    { 	
		boolean bool = false;
		String parameter;
		while(!bool) {
			System.out.println("Quelle action voulez-vous réaliser ? (client/vehicule/reservation/quit)");
			parameter = IOUtils.readString();
			switch (parameter) {
            case "reservation":
					 InterfaceReservation.exec();
                     break;
            case "client":
            		 InterfaceClient.exec();
                     break;
            case "vehicule":
					 InterfaceVehicle.exec();
                     break;
            case "quit":
            		 bool = true;
                     break;
        }
			/*if (parameter.equalsIgnoreCase("reservation"))
				InterfaceReservation.exec();
			else if (parameter.equalsIgnoreCase("client"))
				InterfaceClient.exec();
			else if (parameter.equalsIgnoreCase("vehicule"))
				InterfaceVehicle.exec();
			else if (parameter.equalsIgnoreCase("quit"))
				bool = true;*/
		}
		System.out.println("Fermeture de l'interface !");
	}
}
