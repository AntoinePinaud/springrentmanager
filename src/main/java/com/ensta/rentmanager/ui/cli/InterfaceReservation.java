package com.ensta.rentmanager.ui.cli;

import java.time.LocalDate;
import java.util.List;

import com.ensta.rentmanager.configuration.AppConfiguration;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Reservation;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.utils.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class InterfaceReservation {

    @Autowired
    private ReservationService service;
	
	public static void create() {
        System.out.println("Creation d'une nouvelle réservation");
        int client_id = IOUtils.readInt("Identifiant Client:");
        int vehicule_id = IOUtils.readInt("Identifiant Véhicule:");
        LocalDate debut= IOUtils.readDate("Début de réservation:", true);
        LocalDate fin = IOUtils.readDate("Fin de réservation:", true);
        Reservation reservation = new Reservation(client_id, vehicule_id, debut, fin);
        try {
            ReservationService service = ReservationService.getInstance();
			service.create(reservation);
		} catch (ServiceException e) {
		}	
        }
	
	public static void list() {
        try {
            ReservationService service = ReservationService.getInstance();
			List <Reservation> reservations = service.findAll();
	       for(Reservation reservation : reservations) {
	            System.out.println(reservation);
	        }
		} catch (ServiceException e) {
		}
	}
	
	public static void exec()
    { 
        // Declare the object and initialize with 
        // predefined standard input object  
        System.out.println("Bienvenu dans l'interface réservations !");
        System.out.println("Souhaitez-vous créer une réservation ou lister toutes les réservations? (create/list)");
        // String input 
        boolean bool = false;
        while (!bool) {
        String function = IOUtils.readString(); 
    	if (function.equalsIgnoreCase("create")) {
        	InterfaceReservation.create();
        	bool = true;
        }
        else if (function.equalsIgnoreCase("list")) {
        	InterfaceReservation.list();
        	bool = true;
        }
        else {
        	System.out.println("Entrée incorrecte");
            System.out.println("Souhaitez-vous créer une réservation ou lister toutes les réservations? (create/list)");
        }

        }
        System.out.println("Fin de l'accès aux données");
   }
}
