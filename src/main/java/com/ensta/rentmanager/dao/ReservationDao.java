package com.ensta.rentmanager.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.ensta.rentmanager.dto.ReservationDatabaseDto;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.mapper.ReservationDatabaseMapper;
import com.ensta.rentmanager.model.Reservation;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

@Repository
public class ReservationDao {

	private final Session session;

	private ReservationDatabaseMapper reservationDatabaseMapper;

	@Autowired
	public ReservationDao(Session session, ReservationDatabaseMapper reservationDatabaseMapper) {
		this.session = session;
		this.reservationDatabaseMapper = reservationDatabaseMapper;
	}

	public long create(Reservation reservation) throws DaoException {
		try {
			return (long) session.save(this.reservationDatabaseMapper.mapToDto(reservation));
		} catch (PersistenceException e) {
			System.out.println(e);
			throw new DaoException("Exception in Creation", e);
		}
	}

	public void delete(Reservation reservation) throws DaoException {
		try {

			ReservationDatabaseDto reservation_dto = session.load(ReservationDatabaseDto.class, reservation.getId());
			session.beginTransaction();
			session.delete(reservation_dto);
			session.getTransaction().commit();

		} catch (PersistenceException e) {
			System.out.println(e);
			throw new DaoException("Exception in Deletion", e); }
	}


	public Optional<Reservation> findById(long id) throws DaoException {
		try {
			ReservationDatabaseDto reservationDatabaseDto = session.find(ReservationDatabaseDto.class, id);
			return Optional.ofNullable(this.reservationDatabaseMapper.mapToEntity(reservationDatabaseDto));
		} catch (PersistenceException e)
		{
			throw new DaoException("Exception in FindById", e);
		}
	}

	public List<Reservation> findAll() throws DaoException {
		try {
			Query query = session.createQuery("SELECT v FROM ReservationDatabaseDto v");
			List<ReservationDatabaseDto> dbResult = (List<ReservationDatabaseDto>) query.getResultList();
			List<Reservation> output = new ArrayList<>();
			for (ReservationDatabaseDto reservationDatabaseDto : dbResult) {
				output.add(this.reservationDatabaseMapper.mapToEntity(reservationDatabaseDto));
			}
			return output;
		} catch (PersistenceException e) {
			System.out.println("Exception in List - DAO");
			throw new DaoException("Exception in List", e);
		}
	}

	public int count() throws DaoException {
		List<Reservation> list = this.findAll();
		return list.size();
	}
}
