package com.ensta.rentmanager.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.ensta.rentmanager.dto.ClientDatabaseDto;
import com.ensta.rentmanager.dto.ReservationDatabaseDto;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.mapper.ClientDatabaseMapper;
import com.ensta.rentmanager.mapper.ReservationDatabaseMapper;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Reservation;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.PersistenceException;
import javax.persistence.Query;


@Repository
public class ClientDao {

	private final Session session;

	private ClientDatabaseMapper clientDatabaseMapper;
	private ReservationDatabaseMapper reservationDatabaseMapper;

	@Autowired
	public ClientDao(Session session, ClientDatabaseMapper clientDatabaseMapper, ReservationDatabaseMapper reservationDatabaseMapper) {
		this.session = session;
		this.clientDatabaseMapper = clientDatabaseMapper;
		this.reservationDatabaseMapper = reservationDatabaseMapper;
	}

	public long create(Client client) throws DaoException {
		try {
			return (long) session.save(this.clientDatabaseMapper.mapToDto(client));
		} catch (PersistenceException e) {
			System.out.println(e);
			throw new DaoException("Exception in Creation", e);
		}
	}

	public void delete(Client client) throws DaoException {
		try {

		ClientDatabaseDto client_dto = session.load(ClientDatabaseDto.class, client.getId());
		session.beginTransaction();
		session.delete(client_dto);
		session.getTransaction().commit();

	} catch (PersistenceException e) {
			System.out.println(e);
		throw new DaoException("Exception in Deletion", e); }
	}


	public Optional<Client> findById(long id) throws DaoException {
		try {
			ClientDatabaseDto clientDatabaseDto = session.find(ClientDatabaseDto.class, id);
			return Optional.ofNullable(this.clientDatabaseMapper.mapToEntity(clientDatabaseDto));
		} catch (PersistenceException e)
		{
			throw new DaoException("Exception in FindById", e);
		}
	}

	public List<Client> findAll() throws DaoException {
		try {
			Query query = session.createQuery("SELECT v FROM ClientDatabaseDto v");
			List<ClientDatabaseDto> dbResult = (List<ClientDatabaseDto>) query.getResultList();
			List<Client> output = new ArrayList<>();
			for (ClientDatabaseDto clientDatabaseDto : dbResult) {
				output.add(this.clientDatabaseMapper.mapToEntity(clientDatabaseDto));
			}
			return output;
		} catch (PersistenceException e) {
			System.out.println("Exception in List - DAO");
			throw new DaoException("Exception in List", e);
		}
	}

	public List<Reservation> findReservations(long id) throws DaoException {
		try {
			Query query = session.createQuery("FROM ReservationDatabaseDto where client_id = :id").setParameter("id", id);
			List<ReservationDatabaseDto> dbResult = query.getResultList();
			List<Reservation> output = new ArrayList<>();
			for (ReservationDatabaseDto reservationDatabaseDto : dbResult) {
				output.add(this.reservationDatabaseMapper.mapToEntity(reservationDatabaseDto));
			}
			return output;
		} catch (PersistenceException e) {
			System.out.println("Exception in List - DAO");
			throw new DaoException("Exception in List", e);
		}
	}

	public int count() throws DaoException {
		List<Client> list = this.findAll();
		return list.size();
	}

}
