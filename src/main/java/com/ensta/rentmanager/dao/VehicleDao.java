package com.ensta.rentmanager.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.ensta.rentmanager.dto.VehicleDatabaseDto;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.mapper.VehicleDatabaseMapper;
import com.ensta.rentmanager.model.Vehicule;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

@Component
public class VehicleDao {

	private final Session session;

	private VehicleDatabaseMapper vehicleDatabaseMapper;

	@Autowired
	public VehicleDao(Session session, VehicleDatabaseMapper vehicleDatabaseMapper) {
		this.session = session;
		this.vehicleDatabaseMapper = vehicleDatabaseMapper;
	}

	public long create(Vehicule vehicle) throws DaoException {
		try {
			return (long) session.save(this.vehicleDatabaseMapper.mapToDto(vehicle));
		} catch (PersistenceException e) {
			System.out.println(e);
			throw new DaoException("Exception in Creation", e);
		}
	}

	public void delete(Vehicule vehicule) throws DaoException {
		try {
			VehicleDatabaseDto vehicule_dto = session.load(VehicleDatabaseDto.class, vehicule.getId());
			session.beginTransaction();
			session.delete(vehicule_dto);
			session.getTransaction().commit();
		} catch (PersistenceException e) {
			System.out.println(e);
			throw new DaoException("Exception in Deletion", e); }
	}

	public Optional<Vehicule> findById(long id) throws DaoException {
		try {
			VehicleDatabaseDto vehicleDatabaseDto = session.find(VehicleDatabaseDto.class, id);
			return Optional.ofNullable(this.vehicleDatabaseMapper.mapToEntity(vehicleDatabaseDto));
		} catch (PersistenceException e)
		{
			throw new DaoException("Exception in FindById", e);
		}
	}

	public List<Vehicule> findAll() throws DaoException {
		try {
			Query query = session.createQuery("SELECT v FROM VehicleDatabaseDto v");
			List<VehicleDatabaseDto> dbResult = (List<VehicleDatabaseDto>) query.getResultList();
			List<Vehicule> output = new ArrayList<>();
			for (VehicleDatabaseDto vehicleDatabaseDto : dbResult) {
				output.add(this.vehicleDatabaseMapper.mapToEntity(vehicleDatabaseDto));
			}
			return output;
		} catch (PersistenceException e) {
			System.out.println("Exception in List - DAO");
			throw new DaoException("Exception in List", e);
		}
	}

	public int count() throws DaoException {
		List<Vehicule> list = this.findAll();
		return list.size();
	}
}
