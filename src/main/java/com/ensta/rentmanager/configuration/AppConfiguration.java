package com.ensta.rentmanager.configuration;


import com.ensta.database_utils.persistence.ConnectionManager;
import org.h2.jdbcx.JdbcDataSource;
import org.hibernate.Session;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import javax.sql.DataSource;
import java.sql.SQLException;


@Configuration
@ComponentScan({"com.ensta.rentmanager.service","com.ensta.rentmanager.dao", "com.ensta.rentmanager.mapper", "com.ensta.rentmanager.ui.servlet"})
public class AppConfiguration {
    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan("com.ensta.rentmanager.dto", "com.ensta.rentmanager.model");
        return sessionFactory;
    }

    @Bean
    public DataSource dataSource() {
        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setURL("jdbc:h2:~/h2.database/RentManagerDatabase");
        dataSource.setUser("");
        dataSource.setPassword("");

        return dataSource;
    }

    @Bean
    public Session getSession(LocalSessionFactoryBean localSessionFactoryBean) {
        return localSessionFactoryBean.getObject().openSession();
    }

    @Bean
    public ConnectionManager jdbcConnection() throws SQLException {
        return new ConnectionManager();
    }

}
