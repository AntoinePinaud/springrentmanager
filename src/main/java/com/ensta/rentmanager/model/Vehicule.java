package com.ensta.rentmanager.model;

import javax.persistence.*;

public class Vehicule {

	private long id;
	private String constructeur;
	private int nb_places;
	private String model;
	

	public String getModel() {
		return model;
	}

	public void setModel(String model) {

		this.model = model;
	}

	public Vehicule(long id, String constructeur, String model, int nb_places) {
		this.id = id;
		this.constructeur = constructeur;
		this.model = model;
		this.nb_places = nb_places;
	}
	
	public Vehicule(String constructeur, String model, int nb_places) {
		this.constructeur = constructeur;
		this.model = model;
		this.nb_places = nb_places;
	}

	public Vehicule() {}
	
	public String getConstructeur() {

		return constructeur;
	}
	public void setConstructeur(String constructeur) {

		this.constructeur = constructeur;
	}
	public int getNb_places() {
		return nb_places;
	}
	public void setNb_places(int nb_places) {

		this.nb_places = nb_places;
	}
	
	public long getId() {

		return id;
	}

	public void setId(long id) {

		this.id = id;
	}
	
	@Override
	public String toString() {
		return "Vehicule [id=" + id + ", constructeur=" + constructeur + ", nb_places=" + nb_places + ", model=" + model
				+ "]";
	}

}
