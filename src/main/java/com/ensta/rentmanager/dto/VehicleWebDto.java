package com.ensta.rentmanager.dto;

public class VehicleWebDto {

        private Long id;
        private String model;
        private String constructeur; // format : dd/MM/yyyy
        private int nb_places;

    public VehicleWebDto() {
    }

    public VehicleWebDto(String model, String constructeur, int nb_places) {
        this.model = model;
        this.constructeur = constructeur;
        this.nb_places = nb_places;
    }

    public VehicleWebDto(Long id, String model, String constructeur, int nb_places) {
        this.id = id;
        this.model = model;
        this.constructeur = constructeur;
        this.nb_places = nb_places;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getConstructeur() {
        return constructeur;
    }

    public void setConstructeur(String constructeur) {
        this.constructeur = constructeur;
    }

    public int getNb_places() {
        return nb_places;
    }

    public void setNb_places(int nb_places) {
        this.nb_places = nb_places;
    }
}
