package com.ensta.rentmanager.dto;

import javax.persistence.*;

@Entity
@Table(name = "Vehicule")
public class VehicleDatabaseDto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "modele")
    private String model;
    @Column(name = "constructeur")
    private String constructeur; // format : dd/MM/yyyy
    @Column(name = "nb_places")
    private int nb_places;

    public long getId() {
        return id;
    }

    public VehicleDatabaseDto() {
    }

    public VehicleDatabaseDto(String model, String constructeur, int nb_places) {
        this.model = model;
        this.constructeur = constructeur;
        this.nb_places = nb_places;
    }

    public VehicleDatabaseDto(Long id, String model, String constructeur, int nb_places) {
        this.id = id;
        this.model = model;
        this.constructeur = constructeur;
        this.nb_places = nb_places;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setConstructeur(String constructeur) {
        this.constructeur = constructeur;
    }

    public void setNb_places(int nb_places) {
        this.nb_places = nb_places;
    }

    public String getModel() {
        return model;
    }

    public String getConstructeur() {
        return constructeur;
    }

    public int getNb_places() {
        return nb_places;
    }
}
