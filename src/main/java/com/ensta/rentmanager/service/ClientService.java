package com.ensta.rentmanager.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.ensta.rentmanager.configuration.AppConfiguration;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.dao.ClientDao;
import com.ensta.rentmanager.model.Reservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class ClientService {
	
	private ClientDao clientDao;

	@Autowired
    public ClientService(ClientDao clientDao){

		this.clientDao = clientDao;
    }

	public static ClientService getInstance(){
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
		ClientService service = context.getBean(ClientService.class);
		return service;
	}

	public long create(Client client) throws ServiceException {
		
		if (client.getNom() == "" || client.getPrenom() == "") {
			throw new ServiceException("Le nom ou le prenom du client n'est pas renseigné !");
			}
		else if (client.getNaissance().getYear() >= 2002) {
			throw new ServiceException("Le nom ou le client n'est pas majeur !");
		}
			long id;
			try { 
				client.setNom(client.getNom().toUpperCase());
				id = clientDao.create(client);
				return id;
			} 
			catch (DaoException e) {
				System.out.println(e);
			throw new ServiceException("Une erreur a eu lieu lors de la création du client"); }
	}

	public void delete(Client client) throws ServiceException {

		try {
			clientDao.delete(client);
		}
		catch (DaoException e) {
			throw new ServiceException("Une erreur a eu lieu lors de la suppression du client"); }
	}

	public Client findById(long id) throws ServiceException {
		try {
			Optional<Client> client;
			client = clientDao.findById(id);
			if (client.isPresent()) {
				return client.get();
			}
			else {
				return null;
			}
		} 
		catch (DaoException e) {
		throw new ServiceException("Une erreur a eu lieu lors de la récupération du client"); }
	}

	public List<Client> findAll() throws ServiceException {
		try {
			List<Client> list = new ArrayList<Client>();
			list = clientDao.findAll();
			return list;
		} 
		catch (DaoException e) {
		throw new ServiceException("Une erreur a eu lieu lors de la récupération de la liste de clients"); }
	}

	public int count() throws DaoException {
		return clientDao.count();
	}

	public List<Reservation> findReservations(long id) throws DaoException {
		return clientDao.findReservations(id);
	}
	
}
