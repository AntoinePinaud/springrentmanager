package com.ensta.rentmanager.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.ensta.rentmanager.configuration.AppConfiguration;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Vehicule;
import com.ensta.rentmanager.dao.VehicleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class VehicleService {

	private VehicleDao vehicleDao;

	@Autowired
    public VehicleService(VehicleDao vehicleDao){

		this.vehicleDao = vehicleDao;
    }

    public static VehicleService getInstance(){
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
		VehicleService service = context.getBean(VehicleService.class);
		return service;
	}

	public long create(Vehicule vehicle) throws ServiceException {
		if (vehicle.getConstructeur() == "" || vehicle.getNb_places() <= 1) {
			throw new ServiceException("Le nom du constructeur n'est pas renseigné ou le nombre de place est infèrieur à 2");
			}
			long id;
			try { 
				id = vehicleDao.create(vehicle);
				return id;
			} 
			catch (DaoException e) {
			throw new ServiceException("Une erreur a eu lieu lors de la création du véhicule"); }
	}

	public void delete(Vehicule vehicle) throws ServiceException {

		try {
			vehicleDao.delete(vehicle);
		}
		catch (DaoException e) {
			throw new ServiceException("Une erreur a eu lieu lors de la suppression du véhicule"); }
	}


	public Vehicule findById(long id) throws ServiceException {
		try {
			Optional<Vehicule> vehicule;
			vehicule = vehicleDao.findById(id);
			if (vehicule.isPresent()) {
				return vehicule.get();
			}
			else {
				return null;
			}
		} 
		catch (DaoException e) {
		throw new ServiceException("Une erreur a eu lieu lors de la récupération du véhicule"); }
	}

	public List<Vehicule> findAll() throws ServiceException {
		try {
			List<Vehicule> list = new ArrayList<Vehicule>();
			list = vehicleDao.findAll();
			return list;
		} 
		catch (DaoException e) {
		System.out.println(e);
		throw new ServiceException("Une erreur a eu lieu lors de la récupération de la liste des véhicules"); }
	}

	public int count() throws DaoException {
		return vehicleDao.count();
	}
	
}
