package com.ensta.rentmanager.service;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.ensta.rentmanager.configuration.AppConfiguration;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Reservation;
import com.ensta.rentmanager.dao.ReservationDao;
import com.ensta.rentmanager.model.Vehicule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class ReservationService {

	private ReservationDao reservationDao;

	@Autowired
    private ReservationService(ReservationDao reservationDao){
		this.reservationDao = reservationDao;
    }

	public static ReservationService getInstance(){
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
		ReservationService service = context.getBean(ReservationService.class);
		return service;
	}

	public long create(Reservation reservation) throws ServiceException {

		if (reservation.getFin().isBefore(reservation.getDebut())) {
			throw new ServiceException("Les dates de réservation ne sont pas conformes !");
		}
		long id;
		try {
			id = reservationDao.create(reservation);
			return id;
		}
		catch (DaoException e) {
			throw new ServiceException("Une erreur a eu lieu lors de la création de la réservation"); }
	}

	public void delete(Reservation reservation) throws ServiceException {
		try {
			reservationDao.delete(reservation);
		}
		catch (DaoException e) {
			throw new ServiceException("Une erreur a eu lieu lors de la suppression de la réservation"); }
	}

	public List<Reservation> findAll() throws ServiceException {
		try {
			List<Reservation> list = new ArrayList<Reservation>();
			list = this.reservationDao.findAll();
			return list;
		} 
		catch (DaoException e) {
		throw new ServiceException("Une erreur a eu lieu lors de la récupération de la liste des réservations"); }
	}

	public Reservation findById(long id) throws ServiceException {
		try {
			Optional<Reservation> reservation;
			reservation = reservationDao.findById(id);
			if (reservation.isPresent()) {
				return reservation.get();
			}
			else {
				return null;
			}
		}
		catch (DaoException e) {
			throw new ServiceException("Une erreur a eu lieu lors de la récupération du véhicule"); }
	}

	public int count() throws DaoException {
		return reservationDao.count();
	}
	
}
