# SpringRentManager

ASI-311 - ENSTA  
Antoine PINAUD  
Projet RentManager  
13/11/2019

## Application

Cette application WEB permet de gérer la réservation de véhicules par des particuliers.

## Fonctionnalités

### Véhicules

 - Afficher la liste des véhicules présents dans la base de données
 - Insérer un véhicule dans la base de données
 - Supprimer un véhicule

### Clients

- Afficher la liste des clients présents dans la base de données
- Insérer un client dans la base de données. La date de naissance de ce dernier doit être saisie sous forme de String (ex : 10/01/2019) et être convertie en LocalDate.
- Supprimer un client

### Page d’accueil

- Afficher le nombre de véhicules présents dans la base de données
- Afficher le nombre d’utilisateurs présents dans la base de données
- Sur toutes ces pages, afficher un message d’erreur si quelque chose ne se passe pas comme prévu (exemple : validation des données, envoi d’une requête à la base de données)
- Afficher le nombre de réservations présentes dans la base de données

### Réservations

- Afficher la liste des réservations présentes dans la base de données
- Insérer une réservation dans la base de données
    - Afficher la liste des véhicules dans une liste déroulante afin que l’utilisateur puisse la sélectionner facilement
    - Afficher la liste des clients dans une liste déroulante afin que l’utilisateur puisse le sélectionner facilement
- Supprimer une réservation

### Créer la page de profil d’un client

- Afficher le nombre de réservations d’un client
- Afficher les réservations d’un client

## Fonctionnalités non développées

### Couplage Véhicule/Propriétaire

- Modifier la base de données pour lier une voiture à un client (son propriétaire)
- Modifier le formulaire d’insertion de véhicule pour permettre à l’utilisateur de sélectionner le propriétaire du véhicule
- Page de profil d’un client
    - Afficher le nombre de véhicules appartenant à ce client
    - Afficher les véhicules appartenant à ce client

## Beugs non résolus

### Gestion des réservations

- Erreurs lors de la création et la suppression de certaines réservations.

  
    
###### Projet terminé le 12/11/2019